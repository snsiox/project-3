package project3;

// Big static class for holding the wonderful
// global state of stats for the simulation
class Stats {
    static public int numIn = 0;
    static public int numSuccess = 0;
    static public int leftLine = 0;
    static int[] tableQueueMax;
    static public int boothQueueMax = 0;

    static public int ticksElapsed = 0;

    static public Integer totalVoterTime = 0;

    static public void reset(int numTables) {
        numIn = 0;
        numSuccess = 0;
        leftLine = 0;
        tableQueueMax = new int[numTables];
        boothQueueMax = 0;
        ticksElapsed = 0;
        totalVoterTime = 0;
    }
}