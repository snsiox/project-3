package project3;

import java.util.*;
import java.lang.Math;

// Distributes voters to different desitnations
// based on their name
class SpecialDistributor extends TimedEvent implements InputPoint,
        VoterContainer {

    public ArrayList<InputPoint> outputs = new ArrayList<>();

    // Because InputPoint.canRecv() does not get passed
    // the voter, we cannot know if we have an open
    // destination, so we need to store the voter for
    // a clock cycle
    public Voter person;

    InputPoint currentOutput;

    public SpecialDistributor(InputPoint... outputs) {
        for (InputPoint output : outputs)
            this.outputs.add(output);
    }

    public Boolean canRecv() {
        return person == null;
    }

    public void receive(Voter v) {
        v.container = this;

        int index = (int) Math.round(v.name * outputs.size());

        // This bit makes the probabilities even, but
        // the distribution is all funny now. Not a
        // problem since we never actually show the
        // name to the user.
        if (index == outputs.size())
            index = 0;

        currentOutput = outputs.get(index);

        person = v;

        // Force this TimedEvent to be called next
        // clock tick, so we can mess with the timings
        listener.forceTick();
    }

    public void addOutput(InputPoint output) {
        if (!outputs.contains(output))
            outputs.add(output);
    }

    public void removeOutput(InputPoint output) {
        outputs.remove(output);
    }


    public int event() {
        if (person == null)
            listener.disable();
        else if (currentOutput.canRecv()) {
            person.container = null;
            currentOutput.receive(person);
            person = null;
            listener.disable();
        } else
            // No open destinations, tell the clock to
            // call agin next tick so we can check again
            listener.enable();

        return 0;
    }

    public void removeVoter(Voter v) {
        if (v == person) {
            person.container = null;
            person = null;
        }
    }
}