package project3;

// A generic "place voters can go"
interface InputPoint {
    // Take in a voter and do whatever with them
    public void receive(Voter v);

    // For determining if Voters can be sent here
    public Boolean canRecv();
}