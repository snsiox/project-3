package project3;

import java.util.HashSet;

class Clock {
    private HashSet<ClockListener> listeners = new HashSet<>();

    private HashSet<ClockListener> toBeRemoved = new HashSet<>();

    private HashSet<ClockListener> toBeAdded = new HashSet<>();

    private int lastTick = 0;

    /**
     * constructor
     * makes a new clock listener and adds it to listeners
     *
     * @param events
     */
    public Clock(TimedEvent... events) {
        for (TimedEvent e : events)
            this.listeners.add(new ClockListener(e, 0, 0, this));
    }

    /**
     * runs a multitude of ticks until ending time
     *
     * @param endingTime
     */
    public void run(int endingTime) {
        for (int currentTime = 0; currentTime <= endingTime;
             currentTime++)
            tick(currentTime);
    }

    /**
     * a single tick utilizes the listeners and keeps the line moving
     *
     * @param tickCount
     */
    public void tick(int tickCount) {
        Stats.ticksElapsed++;

        for (ClockListener listener : toBeAdded) //adds stuff from list
            listeners.add(listener);
        toBeAdded.clear(); //clears that list

        for (ClockListener listener : toBeRemoved) //removes stuff
            listeners.remove(listener);
        toBeRemoved.clear(); //clears list

        for (ClockListener listener : listeners) //remaining listeners
            listener.tick(tickCount);

        lastTick = tickCount;
    }

    /**
     * this tick does stuff
     */
    public void tick() {
        tick(lastTick + 1);
    }

    /**
     * @param e
     * @param delay add a timed event with a new clock listener, and set that
     *              listener in the list of things to be added
     */
    public void add(TimedEvent e, int delay) {
        ClockListener listener = new ClockListener(e, delay, lastTick,
                this);
        toBeAdded.add(listener);
    }

    /**
     * @param e adds an event
     */
    public void add(TimedEvent e) {
        add(e, 0);
    }

    /**
     * @param listener removes a listener (look at tick)
     */
    public void remove(ClockListener listener) {
        toBeRemoved.add(listener);
    }

    /**
     * returns the last tick
     *
     * @return lastTick
     */
    public int now() {
        return lastTick;
    }
}
