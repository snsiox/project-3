package project3;

// Measures out time to call a TimedEvent on time
class ClockListener {
    // Used to measure how long it took when a Voter
    // "leaves"
    private int creationTick;

    private int tickDelay;
    private int nextTick;

    private Clock clock;

    public TimedEvent event;

    private Boolean forced = false;
    private Boolean enabled = true;

    public ClockListener(TimedEvent e, int delay, int now, Clock clock) {
        tickDelay = delay;
        creationTick = now;
        nextTick = now + tickDelay;
        this.clock = clock;
        event = e;

        event.listener = this;
    }

    // Called by the Clock, if the TimedEvent is
    // due, call it. The return value sets the
    // delay before the next call
    public void tick(int tick) {
        if ((enabled && (tick >= nextTick)) || forced) {
            int nextDelay = event.event();
            tickDelay = (nextDelay >= 0) ? nextDelay : tickDelay;
            nextTick = tick + tickDelay;
        }
        forced = false;
    }

    // If this is called, it gives the TimedEvent
    // an opportunity to change the tick delay
    // before the next tick it would normally
    // be called
    public void forceTick() {
        forced = true;
    }

    public void enable() {
        enabled = true;
    }

    public void disable() {
        enabled = false;
    }

    public void remove() {
        clock.remove(this);
    }

    // This only sets the delay, does not update
    // when the next call tick is, and the event
    // call can overwrite this anyway
    public void setDelay(int delay) {
        tickDelay = delay;
    }

    public int getCreationTick() {
        return creationTick;
    }

    public Clock getClock() {
        return clock;
    }
}
