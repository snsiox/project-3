package project3;

//import org.jetbrains.annotations.Contract;

import org.omg.CORBA.Object;
import org.omg.PortableServer.LIFESPAN_POLICY_ID;

import javax.swing.JOptionPane;
import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

/**
 * Created by Ethan Carter 10/19/2016.
 */
class SimPanel extends JPanel {
    /**
     * -----------------------------------------------------------------
     * Input
     * -----------------------------------------------------------------
     */
    private int Seconds_To_Next_Person, Average_Seconds_for_CheckIn,
            Total_Time_in_Seconds, Average_Seconds_for_Voting,
            Seconds_Before_Person_leaves, Number_of_Booths;

    private JLabel L_Input_Information, L_Seconds_To_Next_Person,
            L_Average_Seconds_for_CheckIn,
            L_Total_Time_in_Seconds, L_Average_Seconds_for_Voting,
            L_Seconds_Before_Person_leaves, L_Number_of_Booths;

    private JTextField T_Seconds_To_Next_Person,
            T_Average_Seconds_for_CheckIn,
            T_Total_Time_in_Seconds, T_Average_Seconds_for_Voting,
            T_Seconds_Before_Person_leaves, T_Number_of_Booths;

    /**
     * -----------------------------------------------------------------
     * Output
     * -----------------------------------------------------------------
     */
    private int Throughput, MAX, Average_Time, Number_of_People_Left,
            Max_Q_AL, Max_Q_MZ, Max_Q_Voting_Booth_Line;

    private JLabel L_Output_Information, L_Throughput, L_Average_Time,
            L_Number_of_People_Left, L_Max_Q_AL, L_Max_Q_MZ,
            L_Max_Q_Voting_Booth_Line;

    private JLabel T_Throughput, T_Average_Time,
            T_Number_of_People_Left, T_Max_Q_AL, T_Max_Q_MZ,
            T_Max_Q_Voting_Booth_Line;

    /**
     * -----------------------------------------------------------------
     * Other Variables
     * -----------------------------------------------------------------
     */
    private Timer timer;
    private Simulation s;
    private JButton start, quit;
    private GridLayout gridLayout;
    private JLabel spacer = new JLabel
            ("---------------------------------------------------------"
                    + "-----------------");
    private JLabel spacer2 = new JLabel
            ("---------------------------------------------------------"
                    + "-----------------");

    public SimPanel() {
        gridLayout = new GridLayout(0, 2);
        setLayout(gridLayout);
        TextListener text = new TextListener();
        ButtonListener button = new ButtonListener();

        /***************************************************************
         * TOP Half
         **************************************************************/
        //row1
        L_Input_Information = new JLabel("   Input Information");
        add(L_Input_Information);
        add(spacer);

        //row2
        L_Seconds_To_Next_Person = new JLabel("   Second to Next " +
                "Person");
        add(L_Seconds_To_Next_Person);
        T_Seconds_To_Next_Person = new JTextField("20", 15);
        T_Seconds_To_Next_Person.addActionListener(text);
        add(T_Seconds_To_Next_Person);

        //row3
        L_Average_Seconds_for_CheckIn = new JLabel("   Average " +
                "Seconds for CheckIn");
        add(L_Average_Seconds_for_CheckIn);
        T_Average_Seconds_for_CheckIn = new JTextField("15", 15);
        T_Average_Seconds_for_CheckIn.addActionListener(text);
        add(T_Average_Seconds_for_CheckIn);

        //row4
        L_Total_Time_in_Seconds = new JLabel("   Total Time in " +
                "Seconds");
        add(L_Total_Time_in_Seconds);
        T_Total_Time_in_Seconds = new JTextField("10000", 15);
        T_Total_Time_in_Seconds.addActionListener(text);
        add(T_Total_Time_in_Seconds);

        //row5
        L_Average_Seconds_for_Voting = new JLabel("   Average Seconds" +
                " for Voting");
        add(L_Average_Seconds_for_Voting);
        T_Average_Seconds_for_Voting = new JTextField("60", 15);
        T_Average_Seconds_for_Voting.addActionListener(text);
        add(T_Average_Seconds_for_Voting);

        //row6
        L_Seconds_Before_Person_leaves = new JLabel("   Seconds " +
                "Before Person Leaves");
        add(L_Seconds_Before_Person_leaves);
        T_Seconds_Before_Person_leaves = new JTextField("900", 15);
        T_Seconds_Before_Person_leaves.addActionListener(text);
        add(T_Seconds_Before_Person_leaves);

        //row7
        L_Number_of_Booths = new JLabel("   Number of Booths");
        add(L_Number_of_Booths);
        T_Number_of_Booths = new JTextField("3", 15);
        T_Number_of_Booths.addActionListener(text);
        add(T_Number_of_Booths);

        /***************************************************************
         * Buttons
         **************************************************************/
        //row8
        start = new JButton("Start Simulation");
        start.addActionListener(button);
        add(start);
        quit = new JButton("Quit Simulation");
        quit.addActionListener(button);
        add(quit);

        /***************************************************************
         * Bottom Half
         **************************************************************/
        //row9
        L_Output_Information = new JLabel("    Output Information");
        add(L_Output_Information);
        add(spacer2);

        //row10
        L_Throughput = new JLabel("    Throughput");
        add(L_Throughput);
        T_Throughput = new JLabel("");
        add(T_Throughput);

        //row11
        L_Average_Time = new JLabel("    Average Time for a Voter");
        add(L_Average_Time);
        T_Average_Time = new JLabel("");
        add(T_Average_Time);

        //row12
        L_Number_of_People_Left = new JLabel("    Number of " +
                "abandoners");
        add(L_Number_of_People_Left);
        T_Number_of_People_Left = new JLabel("");
        add(T_Number_of_People_Left);

        //row13
        L_Max_Q_AL = new JLabel("    Max Queue Length Checkin A-L");
        add(L_Max_Q_AL);
        T_Max_Q_AL = new JLabel("");
        add(T_Max_Q_AL);

        //row14
        L_Max_Q_MZ = new JLabel("    Max Queue Length Checkin M-Z");
        add(L_Max_Q_MZ);
        T_Max_Q_MZ = new JLabel("");
        add(T_Max_Q_MZ);

        //row15
        L_Max_Q_Voting_Booth_Line = new JLabel("    Max Queue length " +
                "Voting Booth Line");
        add(L_Max_Q_Voting_Booth_Line);
        T_Max_Q_Voting_Booth_Line = new JLabel("");
        add(T_Max_Q_Voting_Booth_Line);

        s = new Simulation();
        refreshParams();
    }

    /*******************************************************************
     * Listener for buttons only. The if's have been left open ended
     * in case you start adding buttons.
     ******************************************************************/
    private class ButtonListener implements ActionListener {
        public void actionPerformed(ActionEvent event) {
            try {
                if (event.getSource() == start) {
                    Stats.reset(Number_of_Booths);
                    s.run(Total_Time_in_Seconds);
                    displayStats();
                    refreshParams();
                } else if (event.getSource() == quit) {
                    System.exit(0);
                }
            } catch (Exception e) {
                String[] options = {"OK", "CANCEL"};
                JOptionPane.showOptionDialog(null,
                        " Input must be an an Integer and less than " +
                                "2,147,483,647. Click OK to " +
                                "continue", "Error",
                        JOptionPane.DEFAULT_OPTION,
                        JOptionPane.WARNING_MESSAGE,
                        null, options, options[0]);
            }
        }
    }

    /*******************************************************************
     * Listener for text fields only. Takes inputs and changes local
     * variables to them
     ******************************************************************/
    private class TextListener implements ActionListener {
        public void actionPerformed(ActionEvent event) {
            try {
                if (event.getSource() ==
                        T_Seconds_To_Next_Person)
                    s.setTimeBetweenVoters(getInt
                            (T_Seconds_To_Next_Person.getText()));
                else if (event.getSource() ==
                        T_Average_Seconds_for_CheckIn)
                    s.setAvgCheckinTime(getInt
                            (T_Average_Seconds_for_CheckIn.getText()));
                else if (event.getSource() == T_Total_Time_in_Seconds)
                    Total_Time_in_Seconds = getInt
                            (T_Total_Time_in_Seconds.getText());
                else if (event.getSource() ==
                        T_Average_Seconds_for_Voting)
                    s.setAvgBoothTime(getInt
                            (T_Average_Seconds_for_Voting.getText()));
                else if (event.getSource() ==
                        T_Seconds_Before_Person_leaves)
                    s.setAvgLeaveTime(getInt
                            (T_Seconds_Before_Person_leaves.getText()));
                else if (event.getSource() == T_Number_of_Booths) {
                    //Number_of_Booths = getInt(T_Number_of_Booths.getText());
                    //s = new Simulation(Number_of_Booths, 2);
                    refreshParams();
                }
            } catch (Exception e) {
//                JOptionPane pane = new JOptionPane();
//                pane.setName("Error");
//                pane.setMessageType(JOptionPane.ERROR_MESSAGE);
//                pane.setOptionType(JOptionPane.DEFAULT_OPTION);
                String[] options = {"OK", "CANCEL"};
                JOptionPane.showOptionDialog(null,
                        "Click OK to continue", "Input must be an " +
                                "integer",
                        JOptionPane.DEFAULT_OPTION,
                        JOptionPane.WARNING_MESSAGE,
                        null, options, options[0]);
                throw new IllegalArgumentException("Input must be an " +
                        "integer");
            }
        }
    }

    /*******************************************************************
     * Helper method that converts a text field answer into an int
     * @return
     ******************************************************************/
    private int getInt(String source) {
        return Integer.parseInt(source);
    }

    /*******************************************************************
     * Helper method that changes all the output labels to
     * appropriate text
     ******************************************************************/

    public void displayStats() {


        //Set output values
        T_Throughput.setText(Stats.numSuccess + " people");

        int avgTime;
        if (Stats.numSuccess == 0) avgTime = 0;
        else avgTime = Stats.totalVoterTime / Stats.numSuccess;
        T_Average_Time.setText(avgTime + " seconds (" +
                (avgTime - s.getAvgBoothTime() - s.getAvgCheckinTime()) +
                " seconds waiting)");

        T_Number_of_People_Left.setText(Stats.leftLine + " "
                + "people (" + (int) Math.round(
                100 * ((float) Stats.leftLine) / ((float) Stats.numIn))
                + "%)");
        T_Max_Q_AL.setText(Stats.tableQueueMax[0] + " people");
        T_Max_Q_MZ.setText(Stats.tableQueueMax[1] + " people");
        T_Max_Q_Voting_Booth_Line.setText(Stats.boothQueueMax +
                " people");
    }

    public void refreshParams() {
        Number_of_Booths = getInt
                (T_Number_of_Booths.getText());
        s = new Simulation(Number_of_Booths, 2);
        s.setTimeBetweenVoters(getInt
                (T_Seconds_To_Next_Person.getText()));
        s.setAvgCheckinTime(getInt
                (T_Average_Seconds_for_CheckIn.getText()));
        Total_Time_in_Seconds = getInt
                (T_Total_Time_in_Seconds.getText());
        s.setAvgBoothTime(getInt
                (T_Average_Seconds_for_Voting.getText()));
        s.setAvgLeaveTime(getInt
                (T_Seconds_Before_Person_leaves.getText()));
    }

    public static void main(String[] args) {
        JFrame frame = new JFrame("Voting Simulation");
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

        SimPanel panel = new SimPanel();
        frame.getContentPane().add(panel);
        frame.setPreferredSize(new Dimension(600, 400));
        frame.setResizable(false);
        frame.pack();
        frame.setVisible(true);
    }
}
