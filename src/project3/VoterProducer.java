package project3;

import java.util.Random;


class VoterProducer extends TimedEvent {

    private InputPoint output;
    private int averageBoothTime;
    private int averageCheckinTime;
    private int averageLeaveTime;
    private int id;
    private int voterid;

    private Random r = new Random();

    public VoterProducer(InputPoint output,
                         int averageBoothTime,
                         int averageCheckinTime,
                         int averageLeaveTime, int id) {

        this.output = output;
        this.averageBoothTime = averageBoothTime;
        this.averageCheckinTime = averageCheckinTime;
        this.averageLeaveTime = averageLeaveTime;
        this.id = id;
    }

    public Voter send() {
        // This way, the last digit of the voter's
        // id is the id of the producer that made it
        Voter person = new Voter(voterid++ * 10 + id);

        // Random number for determining the voter
        // type. Range of 0-9, since we need 10%
        // probability resolution
        int typeSpec = r.nextInt(9);

        // By default the delay times are unchanged
        double bMul = 1;
        double cMul = 1;
        double lMul = 1;

        if (typeSpec <= 6)            // 0-6 => 70%
            person.setType(VoterType.NORMAL);
        else if (typeSpec == 9) {    // 9   => 10%
            person.setType(VoterType.SPECIAL);
            bMul = 3;
            cMul = 1.5;
            lMul = 2;
        } else {                    // 7-8 => 20%
            person.setType(VoterType.LIMITED);
            bMul = 0.5;
            lMul = 0.5;
        }

        person.setDelayTime(DelayTime.BOOTH_TIME,
                (int) (bMul * centeredRandom(averageBoothTime, 0.5)));

        person.setDelayTime(DelayTime.CHECKIN_TIME,
                (int) (cMul * centeredRandom(averageCheckinTime, 0.5)));

        int timeToLeave =
                (int) (lMul * centeredRandom(averageLeaveTime, 0.5));

        // Add to clock with timeout of the amount of time
        // before the voter gets bored and leaves
        listener.getClock().add(person, timeToLeave);

        person.name = r.nextDouble(); // lol

        person.container = null;

        Stats.numIn++;

        return person;
    }

    public Boolean canSend() {
        // We can always produce a voter. Not that it
        // matters. There is positive voter pressure
        // from the VoterProducer so this never gets
        // called
        return true;
    }

    // Called by the clock listener
    public int event() {
        if (output.canRecv())
            output.receive(send());
        return -1;
    }

    public int getAvgBoothTime() {
        return averageBoothTime;
    }

    public void setAvgBoothTime(int avgBoothTime) {
        averageBoothTime = avgBoothTime;
    }

    public int getAvgCheckinTime() {
        return averageCheckinTime;
    }

    public void setAvgCheckinTime(int avgCheckinTime) {
        averageCheckinTime = avgCheckinTime;
    }

    public int getAvgLeaveTime() {
        return averageLeaveTime;
    }

    public void setAvgLeaveTime(int avgLeaveTime) {
        averageLeaveTime = avgLeaveTime;
    }

    public int getProducerId() {
        return id;
    }

    public void setProducerId(int id) {
        this.id = id;
    }

    public void resetProducer() {
        // The id of the voters is the only state
        // we need to reset
        voterid = 0;
    }

    // spread was effectively 0.5 in the provided
    // code, and 0.1 on the rubriccy thing. With
    // a Gaussian distribution we will occasionally
    // get negative values regardless, which are not
    // a problem for us, they are just effectively
    // zero. Not terribly realistic, but it works
    private int centeredRandom(int center, double spread) {
        return (int) (center * spread * r.nextGaussian() + center + .5);
    }
}
