package project3;

import java.util.EnumMap;

class Voter extends TimedEvent {

    private EnumMap<DelayTime, Integer> delayTimes =
            new EnumMap<>(DelayTime.class);

    private VoterType type;

    public int id;

    // That's right, their name is a number, strictly
    // between the values of 0.0 and 1.0. We can do
    // this because the name is never displayed to
    // the user, and it allows us to evenly divide
    // the Voters among any number of check-in tables
    // efficiently
    public double name;

    public VoterContainer container;

    public Voter(int id) {
        this.id = id;
    }

    public int getDelayTime(DelayTime delaySpec) {
        return delayTimes.get(delaySpec);
    }

    public void setDelayTime(DelayTime delaySpec, int delay) {
        delayTimes.put(delaySpec, delay);
    }

    public int event() {    // Only called when leaveTime has elapsed
        Stats.leftLine++;
        delete();
        return -1;
    }

    public void delete() {
        listener.remove();                    // remove from clock
        if (container != null)
            container.removeVoter(this);    // remove from container
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public double getName() {
        return name;
    }

    public void setName() {
        this.name = name;
    }

    public VoterContainer getContainer() {
        return container;
    }

    public VoterType getType() {
        return type;
    }

    public void setType(VoterType type) {
        this.type = type;
    }
}

// Different places where the voter can have
// a different time delay. LEAVE_TIME is unused
enum DelayTime {
    BOOTH_TIME, CHECKIN_TIME, LEAVE_TIME
}

// Would be useful for representing the different
// types in a graphically different way.
enum VoterType {
    NORMAL, LIMITED, SPECIAL
}