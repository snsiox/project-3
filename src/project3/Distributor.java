package project3;

import java.util.*;

// Distributes voters to a number of possible destinations
class Distributor implements InputPoint {

    // List of possible outputs
    public ArrayList<InputPoint> outputs = new ArrayList<>();

    // Stores the available destination for sending
    // after canRecv() identifies it
    public InputPoint nOutput;

    // Stores the index of the last used output,
    // so we can continue from there with the next voter
    public int outputIndex;

    // Variadic constructors FTW
    public Distributor(InputPoint... outputs) {
        for (InputPoint output : outputs)
            this.outputs.add(output);
    }

    // Overloaded from InputPoint, checks if any
    // destinations can accept a voter, and selects
    // the first one that can.
    public Boolean canRecv() {
        Boolean outputAvailable = false;

        ListIterator<InputPoint> iterator =
                outputs.listIterator(outputIndex);

        while (iterator.hasNext()) {
            nOutput = iterator.next();
            if (nOutput.canRecv()) {
                outputAvailable = true;
                break;
            }
        }
        outputIndex = iterator.hasNext() ? iterator.nextIndex() : 0;

        return outputAvailable;
    }

    // When receiving a voter, instantly transfer it
    // to the previously identified destination
    public void receive(Voter v) {
        nOutput.receive(v);
    }

    public void addOutput(InputPoint output) {
        if (!outputs.contains(output))
            outputs.add(output);
    }

    public void removeOutput(InputPoint output) {
        outputs.remove(output);
    }

}