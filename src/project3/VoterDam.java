package project3;

// Super shady class to simulate a backlog
// Incredibly useful for testing
class VoterDam implements InputPoint {
    public Boolean canRecv() {
        return false;
    }

    public void receive(Voter v) {
        return; // This should never happen, but
        // this is programming. Who knows?
    }
}