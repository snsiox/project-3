package project3;

import java.util.ArrayList;

// A line of people. What are we, British?
class Queue extends TimedEvent implements InputPoint, VoterContainer {
    // INFINITE LENGTH
    private ArrayList<Voter> voters = new ArrayList<>();

    public InputPoint output;

    // For tracking the maximum length during
    // the simulation
    public int maxLen = 0;

    public Queue(InputPoint output) {
        this.output = output;
    }

    public void receive(Voter v) {
        voters.add(v);
        v.container = this;
        if (length() > maxLen)
            maxLen = length();
    }

    public Boolean canRecv() {
        // There's plenty of room
        // at the Hotel California
        return true;
    }

    public Voter send() {
        // You can check out
        // Anytime you like
        Voter v = voters.remove(0);
        // But you can never leave
        v.container = null;
        return v;
    }

    public Boolean canSend() {
        return length() > 0;
    }

    public int length() {
        return voters.size();
    }

    public int event() {
        // "Relax," said the night man
        // We are programmed to receive
        if (output.canRecv() && canSend())
            output.receive(send());

        return 0;
    }

    public void removeVoter(Voter v) {
        // Last thing I remember
        // I was running for the door
        voters.remove(v);
    }

    public void setOutput(InputPoint output) {
        // I had to find the passage
        // back to the place I was before
        this.output = output;
    }
}