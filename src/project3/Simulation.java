package project3;

import java.util.ArrayList;

// Takes care of setting up and running the simulation
class Simulation {
    private int avgBoothTime = 60;
    private int avgCheckinTime = 15;
    private int avgLeaveTime = 900;
    private int ticksBetweenVoters = 20;
    private VoterProducer producer;

    private SpecialDistributor nameSplitter;

    private ArrayList<Queue> tableQueues = new ArrayList<>();

    // Two tables, very simple to modify,
    // but we don't from the GUI
    private int numTables = 2;
    private ArrayList<Table> tables = new ArrayList<>();

    private Queue boothQueue;

    private Distributor boothDistributor;

    // The voting booths are just sneakily-specific
    // instances of the check-in tables
    private int numBooths = 3;
    private ArrayList<Table> booths = new ArrayList<>();

    // Sounds ominous, but the voters need to go
    // *somewhere* when they leave the "booth"
    private VoterDeleter exit;

    private Clock clock;

    public Simulation(int numBooths, int numTables) {
        this.numTables = numTables;
        this.numBooths = numBooths;
        regen();
    }

    public Simulation() {
        regen();
    }

    // Literally just reconstruct the whole thing.
    // Before this we were modifying only parts of
    // it, and kept having weird problems. This is
    // not a good fix, but it works.
    public void regen() {
        Stats.reset(numTables);

        clock = new Clock();

        exit = new VoterDeleter();

        boothDistributor = new Distributor();

        while (booths.size() < numBooths)
            booths.add(new Table(DelayTime.BOOTH_TIME, exit));
        while (booths.size() > numBooths)
            booths.remove(0);

        for (Table booth : booths) {
            boothDistributor.addOutput(booth);
            clock.add(booth);
        }

        boothQueue = new Queue(boothDistributor);

        clock.add(boothQueue);

        while (tables.size() < numTables)
            tables.add(new Table(DelayTime.CHECKIN_TIME, boothQueue));
        while (tables.size() > numTables)
            tables.remove(0);

        while (tableQueues.size() < numTables)
            tableQueues.add(new Queue(null));
        while (tableQueues.size() > numTables) {
            tableQueues.remove(0);
        }

        nameSplitter = new SpecialDistributor();

        for (int tableAndQueue = 0; tableAndQueue < numTables;
             tableAndQueue++) {
            tableQueues.get(tableAndQueue)
                    .setOutput(tables.get(tableAndQueue));
            clock.add(tableQueues.get(tableAndQueue));
            clock.add(tables.get(tableAndQueue));

            nameSplitter.addOutput(tableQueues.get(tableAndQueue));
        }

        clock.add(nameSplitter);

        producer = new VoterProducer(nameSplitter, avgBoothTime,
                avgCheckinTime, avgLeaveTime, 0);

        clock.add(producer, ticksBetweenVoters);
    }

    // Most stats get written directly to the static
    // variables in Stats, but the Queues don't know
    // which variable to write to, so we copy the
    // values over to Stats after the simulation
    private void updateStats() {
        for (int tableIndex = 0; tableIndex < tableQueues.size();
             tableIndex++)
            Stats.tableQueueMax[tableIndex] =
                    tableQueues.get(tableIndex).maxLen;

        Stats.boothQueueMax = boothQueue.maxLen;
    }

    public void run(int ticks) {
        clock.run(ticks);
        updateStats();
    }

    // Allows us to run the simulation tick by tick,
    // but because our GUI is really primitive, it's
    // not used.
    public void tick() {
        clock.tick();
        updateStats();
    }

    public int getAvgBoothTime() {
        return avgBoothTime;
    }

    public void setAvgBoothTime(int avgBoothTime) {
        this.avgBoothTime = avgBoothTime;
        this.producer.setAvgBoothTime(avgBoothTime);
    }

    public int getAvgCheckinTime() {
        return avgCheckinTime;
    }

    public void setAvgCheckinTime(int avgCheckinTime) {
        this.avgCheckinTime = avgCheckinTime;
        producer.setAvgCheckinTime(avgCheckinTime);
    }

    public int getAvgLeaveTime() {
        return avgLeaveTime;
    }

    public void setAvgLeaveTime(int avgLeaveTime) {
        this.avgLeaveTime = avgLeaveTime;
        producer.setAvgLeaveTime(avgLeaveTime);
    }

    public int getNumTables() {
        return numTables;
    }

    public void setNumTables(int numTables) {
        this.numTables = numTables;

        regen();
    }

    public int getNumBooths() {
        return numBooths;
    }

    public void setNumBooths(int numBooths) {
        this.numBooths = numBooths;

        regen();
    }

    public int getTimeBetweenVoters() {
        return ticksBetweenVoters;
    }

    public void setTimeBetweenVoters(int timeBetweenVoters) {
        this.ticksBetweenVoters = timeBetweenVoters;

        // Remove and read the listener to the clock
        // so the delay is right
        clock.remove(producer.listener);
        clock.add(producer, ticksBetweenVoters);
    }
}