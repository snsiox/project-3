package project3;

import java.util.ArrayList;

class Table extends TimedEvent implements InputPoint, VoterContainer {

    private Voter person;
    private int completed = 0;
    private DelayTime delaySpec;

    private InputPoint output;

    private Boolean taken = false;
    private Boolean busy = false;

    public Table(DelayTime delaySpec, InputPoint output) {
        this.delaySpec = delaySpec;
        this.output = output;
    }

    /**
     * @param v Table receives Voter v, sets taken to true (table has someone)
     *          the current voter becomes v
     */
    public void receive(Voter v) {
        taken = true;
        person = v;
        person.container = this;
    }

    /**
     * canRecv checks to see if table has already recieved a voter
     * if it has, then taken is true, so it returns false
     * if it has not, then taken is false, so it returns true
     *
     * @return !taken
     */
    public Boolean canRecv() {
        return !taken;
    }

    /**
     * @return delay time of a person,
     */
    public int event() {
        if (taken && !busy) {
            busy = true;
            return person.getDelayTime(delaySpec);
        } else if (taken && busy && output.canRecv()) {
            busy = false;
            taken = false;
            person.container = null;
            output.receive(person);
            person = null;
            completed++;
        }

        return 0;
    }

    /**
     * takes a voter and removes it, simply put.
     * look at comments for more detail
     *
     * @param v
     */
    public void removeVoter(Voter v) {
        if ((person == v) && taken) {
            if (busy) {            // leave partway through voting, lol
                busy = false;
                taken = false;
                person = null;
            } else {
                taken = false;
                person = null;
            }
            listener.forceTick();    // Without this, the clock will
            // continue waiting for the voter
            // to be done
        }
    }

    /**
     * returns the amount of voters coming through a table
     *
     * @return completed
     */
    public int getThroughPut() {
        return completed;
    }

    /**
     * return delaySpec, the variable holding how long it took
     *
     * @return delaySpec
     */
    public DelayTime getDelaySpec() {
        return delaySpec;
    }

    /**
     * sets the current delay for how long it takes
     *
     * @param delaySpec
     */
    public void setDelaySpec(DelayTime delaySpec) {
        this.delaySpec = delaySpec;
    }

    /**
     * output output is the number of people sent from table to booth
     *
     * @return output
     */
    public InputPoint getOutput() {
        return output;
    }

    /**
     * returns the output when called
     *
     * @param output
     */
    public void setOutput(InputPoint output) {
        this.output = output;
    }
}