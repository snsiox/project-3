package project3;

// A thing Voters can be in, and by extension
// a thing voters could need to be removed
// from if they get bored and leave
interface VoterContainer {
    public void removeVoter(Voter v);
}