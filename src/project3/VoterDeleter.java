package project3;

// Another shady but helpful class. It just 
// takes voters after they have voted, and
// quietly kills them. It also keeps track
class VoterDeleter implements InputPoint {
    public Boolean canRecv() {
        return true;
    }

    public void receive(Voter v) {
        Stats.numSuccess++;

        int created = v.listener.getCreationTick();
        int now = v.listener.getClock().now();

        // For calculating the average time to vote
        Stats.totalVoterTime += v.listener.getClock().now() -
                v.listener.getCreationTick();
        v.delete();
    }


}