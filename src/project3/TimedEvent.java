package project3;

public abstract class TimedEvent {
    // This ClockListener is the one responsible
    // for calling the timed event
    public ClockListener listener;

    abstract public int event();    // returns ticks until next call,
    // or -1 to maintain current value
}
